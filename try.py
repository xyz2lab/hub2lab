import requests

url = "https://api.github.com/orgs/fpotter-gitlab-org/repos"
headers = {
    "Accept": "application/vnd.github+json",
    "Authorization": "Bearer YOUR_TOKEN",  # Replace YOUR_TOKEN with your GitHub token
    "X-GitHub-Api-Version": "2022-11-28",
}

response = requests.get(url, headers=headers)

# Check if the request was successful (status code 200)
if response.status_code == 200:
    # Assuming you want to print the JSON response
    print(response.json())
else:
    print(f"Request failed with status code: {response.status_code}")
    print(response.text)
